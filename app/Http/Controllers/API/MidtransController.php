<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Midtrans\Config;
use Midtrans\Notification;

class MidtransController extends Controller
{
    public function callback (Request $request)
    {
        // set Konfig Midtrans
        Config::$serverKey = config('services.midtrans.serverKey');
        Config::$isProduction = config('services.midtrans.serverKey');
        Config::$isSanitized = config('services.midtrans.serverKey');
        Config::$is3ds = config('services.midtrans.serverKey');

        // Buat Instance Midtrans Notif
        $notification = new Notification();
        
        // assign ke var untuk memudahkan coding
        $status = $notification->transaction_status;
        $type = $notification->payment_type;
        $fraud = $notification->fraud_status;
        $order_id = $notification->order_id;

        //cari transaksi berdasarkan ID
        $transaction = Transaction::finOrFail($order_id);

        // Handle Notif status midtrans
        if($status == 'capture') {
            if($type == 'credit_card')
            {
                if($fraud == 'challenge')
                {
                    $transaction->status = 'PENDING';
                }
                else
                {
                    $transaction->status = 'SUCCESS';
                }
            }
            else if($status == 'settlement')
            {
                $transaction->status = 'SUCCESS';
            }
            else if($status == 'pending')
            {
                $transaction->status = 'PENDING';
            }
            else if($status == 'deny')
            {
                $transaction->status = 'CANCELLED';
            }
            else if($status == 'expire')
            {
                $transaction->status = 'CANCELLED';
            }
            else if($status == 'cancle')
            {
                $transaction->status = 'CANCELLED';
            }
        }

        // Simpan Transaksi
        $transaction->save();
    }

    public function success()
    {
        return view('midtrans.success');
    }

    public function unfinish()
    {
        return view('midtrans.unfinish');
    }

    public function error()
    {
        return view('midtrans.error');
    }
}